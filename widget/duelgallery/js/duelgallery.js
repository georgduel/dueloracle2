define(
  
  ['jquery'],
  
  function($) {
    'use strict';
  
    return {
      loadGallery: function(widget) {
        // load gallery settings from widget
        var sort = 'sortOrder' in widget ? widget.sortOrder() : 'created';
        var config = {
          sort: sort,
          color: widget.textColor() || '#222222',
          bgColor: widget.backgroundColor() || '#ffffff'
        };

        var getRule = function(rows, columns, breakpoint) {
          var obj = {
            rule: {
              rows: +rows,
              columns: +columns
            },
            breakpoint: +breakpoint || Number.POSITIVE_INFINITY // is default rule
          };
          if (breakpoint) obj.rule.mediaQuery = '(max-width: ' + breakpoint + 'px)';
          return obj;
        }
        
        var layoutRules = [
          getRule(widget.galleryRows(), widget.galleryColumns())
        ];
        
        var maxBreakpoints = 3;
        var numberRe = /\d+/;
        for (var i = 1; i <= maxBreakpoints; i++) {
          // properties of widget can only be accessed through functions
          var fnName = 'breakPoint' + i;
          // value entered for the breakpoint?
          var text = fnName in widget ? widget[fnName]() : '';
          // is a number present?
          var breakpoint = numberRe.exec(text);
          if (breakpoint) {
            layoutRules.push(getRule(
              widget['galleryRowsBP' + i](),
              widget['galleryColumnsBP' + i](),
              breakpoint[0]
            ));
          }
        }

        // sort by breakpoint, because rules will be tested in order
        layoutRules.sort(function(a, b) { return a.breakpoint - b.breakpoint; });
        config.layoutRules = layoutRules.map(function(el) { return el.rule; });

        if (widget.customGalleryId() !== '') {
          config.id = widget.customGalleryId();
        } else if (
          'product' in widget &&
          widget.product() &&
          (!('duelGalleryEnabled' in widget.product()) || widget.product().duelGalleryEnabled())
        ) {
          var product = widget.product();
          var shortId = widget.site().extensionSiteSettings.duelsettings.duelShortId;

          // fetch gallery from shop's short id and product id
          config.product = shortId + '/' + product.id();

          // get product specific settings (if there are any)
          if ('duelGalleryColor' in product && product.duelGalleryColor()) {
            config.color = product.duelGalleryColor();
          }
          if ('duelGalleryBackgroundColor' in product && product.duelGalleryBackgroundColor()) {
            config.bgColor = product.duelGalleryBackgroundColor();
          }
          if ('duelGalleryRows' in product && +product.duelGalleryRows() > 0) {
            config.rows = +product.duelGalleryRows();
          }
          if ('duelGalleryColumns' in product && +product.duelGalleryColumns() > 0) {
            config.columns = +product.duelGalleryColumns();
          }
          if ('duelGallerySortOrder' in product && product.duelGallerySortOrder()) {
            config.sort = product.duelGallerySortOrder();
          }
        }
          
        if (config.id || config.product) {
          // load script that inserts correct gallery into #duelvision-component in display.template
          if (!window.DuelVision) {
            $.getScript('https://vision.duel.me/loader.js', function() {
              DuelVision.load(config);
            });
          } else {
            DuelVision.load(config);
          }
        }
      }
    };
  }
);

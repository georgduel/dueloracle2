# Duel Gallery Integration v2

The second version of the Duel gallery integration for Oracle Commerce Cloud. You also need this extension in order for the Duel server-side extension to work.

## Steps for installation

1. Create a new extension ID in Settings > Extensions > Developer.
2. Replace **extensionID** in `ext.json` with the one you generated in step 1.
3. Zip all files (not the folder!), excluding the readme & gitignore and upload the extension (run `zip -r duel-oracle.zip config widget ext.json` in the working directory).
4. Navigate to Settings > Duel Settings and add your Duel Short id.
5. Go to Design, find the Product Layout, go into Grid View and drag the Duel Gallery component into the layout.

## Customization

You can adjust the default colors, row & column layout and sort order for each widget in its settings.

To create product specific gallery settings, you need to add properties the Base Product type. These are the supported property IDs:

- duelGalleryEnabled
- duelGalleryColor
- duelGalleryBackgroundColor
- duelGalleryRows
- duelGalleryColumns
- duelGallerySortOrder

## Together with the Duel Server-Side Extension

To tell Duel which products should have a campaign and gallery, you need to create a new (non-navigable) collection with all the products you wish to connect to Duel.

In Settings > Duel Settings you can then find the necessary fields for the server-side extension to retrieve the correct products and currency (Collection ID and Price Group ID).

To check if everything's set up correctly, go to */ccstorex/custom/duel/products* and confirm that the JSON data's correct. You can now paste that URL in the Duel dashboard under Profile > Settings > Product catalog.